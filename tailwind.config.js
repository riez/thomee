const { colors, boxShadow } = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  plugins: [require('@tailwindcss/aspect-ratio')],
  theme: {
    screens: {
      xs: '380px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    colors: {
      ...colors,
      'custom-green': {
        dark: '#319795',
        DEFAULT: '#81e6d9',
      },
      'custom-black': '#2d3748',
      'custom-blue': '#3182ce',
      'custom-gray': {
        DEFAULT: '#718096',
        light: '#CBD5E0',
      },
      primary: '#e6fffa',
      secondary: '#ebf4ff',
      tertiary: '#319795',
    },
    boxShadow: {
      ...boxShadow,
      'custom-shadow': '0 -1px 3px #0003',
    },
    extend: {
      width: {
        'fit-content': 'fit-content',
        inherit: 'inherit',
      },
      maxWidth: {
        'mobile-limit': '480px',
        inherit: 'inherit',
      },
    },
  },
}
